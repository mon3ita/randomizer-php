<?php

namespace Randomizer;

class Randomizer {
    private static $NUMBERS = "0123456789";
    private static $UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static $LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static $PUNCT = ".?!;#,";

    public static function string($length) {
        if($length < 1) {
            throw new Exception("Length can not be with length < 1");
        }

        $random = "";

        $num_length = strlen(self::$NUMBERS);
        $lower_length = strlen(self::$LOWER);
        $upper_length = strlen(self::$UPPER);
        $punct_length = strlen(self::$PUNCT);

        srand(floor(time() / (60*60*24)));
        for($i = 0; $i < $length; $i++) {
            $rnum = rand($lower_length ** 2, $lower_length ** 10);

            switch($rnum % 4) {
                case 0:
                    $random = $random . self::$NUMBERS[$rnum % $num_length];
                    break;
                case 1:
                    $random = $random . self::$UPPER[$rnum % $upper_length];
                    break;
                case 2:
                    $random = $random . self::$LOWER[$rnum % $lower_length];
                    break;
                case 3:
                    $random = $random . self::$PUNCT[$rnum % $punct_length];
            }
        }

        return $random;
    }

    public static function numbers($length) {
        if($length < 1) {
            throw new Exception("Length can not be with length < 1");
        }

        $random = "";

        $num_length = strlen(self::$NUMBERS);

        srand(floor(time() / (60*60*24)));
        for($i = 0; $i < $length; $i++) {
            $rnum = rand($num_length ** 2, $num_length ** 10);

            $random = $random . self::$NUMBERS[$rnum % $num_length];
        }

        return $random;
    }

    public static function chars($length) {
        if($length < 1) {
            throw new Exception("Length can not be with length < 1");
        }

        $random = "";

        $lower_length = strlen(self::$LOWER);
        $upper_length = strlen(self::$UPPER);

        srand(floor(time() / (60*60*24)));
        for($i = 0; $i < $length; $i++) {
            $rnum = rand($lower_length ** 2, $lower_length ** 10);

            switch($rnum % 2) {
                case 0:
                    $random = $random . self::$UPPER[$rnum % $upper_length];
                    break;
                case 1:
                    $random = $random . self::$LOWER[$rnum % $lower_length];
                    break;
            }
        }

        return $random;
    }
}