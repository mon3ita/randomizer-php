# RANDOMIZER

Example usage

```

use Randomizer\Randomizer;

$random_string = Randomizer::string($length);
$random_numbers = Randomizer::numbers($length);
$random_chars = Randomizer::chars($length);
```