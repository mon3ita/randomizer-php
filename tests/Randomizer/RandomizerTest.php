<?php

use PHPUnit\Framework\TestCase;

require __DIR__ . "/../../src/Randomizer/Randomizer.php";
use Randomizer\Randomizer;

class RandomizerTest extends TestCase {

    public function testString() {
        $random = Randomizer::string(10);
        $this->assertEquals(10, strlen($random));
    }

    public function testNumber() {
        $random = Randomizer::numbers(10);
        $this->assertEquals(10, strlen($random));
    }

    public function testChars() {
        $random = Randomizer::chars(10);
        $this->assertEquals(10, strlen($random));
    }
}